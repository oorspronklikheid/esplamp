EESchema Schematic File Version 2
LIBS:esplamp-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:esp-wroom-32
LIBS:esplamp-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L IRFR3708 Q1
U 1 1 5AA41475
P 5500 4300
F 0 "Q1" H 5650 4425 50  0000 L CNN
F 1 "IRFR3708" H 5650 4350 50  0000 L CNN
F 2 "esp32:TO-252" H 5650 4275 50  0000 L CIN
F 3 "" H 5400 4350 50  0000 L CNN
	1    5500 4300
	1    0    0    -1  
$EndComp
$Comp
L ESP-WROOM-32 U1
U 1 1 5AA41E56
P 3950 2200
F 0 "U1" H 3950 2750 60  0000 C CNN
F 1 "ESP-WROOM-32" H 3950 2650 60  0000 C CNN
F 2 "esp32:esp32" H 3700 2050 60  0001 C CNN
F 3 "" H 3700 2050 60  0001 C CNN
	1    3950 2200
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 5AB91873
P 2800 1550
F 0 "C3" H 2825 1650 50  0000 L CNN
F 1 "10uF" H 2825 1450 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D3_P2.5" H 2838 1400 50  0001 C CNN
F 3 "" H 2800 1550 50  0000 C CNN
	1    2800 1550
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 5AB91912
P 2550 1550
F 0 "C2" H 2575 1650 50  0000 L CNN
F 1 "0.1uF" H 2575 1450 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 2588 1400 50  0001 C CNN
F 3 "" H 2550 1550 50  0000 C CNN
	1    2550 1550
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 5AB919AB
P 2000 2400
F 0 "C1" H 2025 2500 50  0000 L CNN
F 1 "0.1uF" H 2025 2300 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 2038 2250 50  0001 C CNN
F 3 "" H 2000 2400 50  0000 C CNN
	1    2000 2400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 5AB91A1E
P 3500 4100
F 0 "#PWR01" H 3500 3850 50  0001 C CNN
F 1 "GND" H 3500 3950 50  0000 C CNN
F 2 "" H 3500 4100 50  0001 C CNN
F 3 "" H 3500 4100 50  0001 C CNN
	1    3500 4100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5AB91A3C
P 2000 2750
F 0 "#PWR02" H 2000 2500 50  0001 C CNN
F 1 "GND" H 2000 2600 50  0000 C CNN
F 2 "" H 2000 2750 50  0001 C CNN
F 3 "" H 2000 2750 50  0001 C CNN
	1    2000 2750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5AB91A5A
P 2800 1850
F 0 "#PWR03" H 2800 1600 50  0001 C CNN
F 1 "GND" H 2800 1700 50  0000 C CNN
F 2 "" H 2800 1850 50  0001 C CNN
F 3 "" H 2800 1850 50  0001 C CNN
	1    2800 1850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 5AB91A78
P 5050 2050
F 0 "#PWR04" H 5050 1800 50  0001 C CNN
F 1 "GND" H 5050 1900 50  0000 C CNN
F 2 "" H 5050 2050 50  0001 C CNN
F 3 "" H 5050 2050 50  0001 C CNN
	1    5050 2050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 5AB91AB9
P 5100 3700
F 0 "#PWR05" H 5100 3450 50  0001 C CNN
F 1 "GND" H 5100 3550 50  0000 C CNN
F 2 "" H 5100 3700 50  0001 C CNN
F 3 "" H 5100 3700 50  0001 C CNN
	1    5100 3700
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR06
U 1 1 5AB91B78
P 2350 1250
F 0 "#PWR06" H 2350 1100 50  0001 C CNN
F 1 "+3V3" H 2350 1390 50  0000 C CNN
F 2 "" H 2350 1250 50  0001 C CNN
F 3 "" H 2350 1250 50  0001 C CNN
	1    2350 1250
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5AB91C63
P 2000 1850
F 0 "R1" V 2080 1850 50  0000 C CNN
F 1 "10k" V 2000 1850 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 1930 1850 50  0001 C CNN
F 3 "" H 2000 1850 50  0000 C CNN
	1    2000 1850
	1    0    0    -1  
$EndComp
NoConn ~ 4800 2700
$Comp
L CONN_01X03 P2
U 1 1 5AB91E1A
P 5750 2400
F 0 "P2" H 5750 2600 50  0000 C CNN
F 1 "CONN_01X03" V 5850 2400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03" H 5750 2400 50  0001 C CNN
F 3 "" H 5750 2400 50  0000 C CNN
	1    5750 2400
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P1
U 1 1 5AB91E71
P 5300 3450
F 0 "P1" H 5300 3600 50  0000 C CNN
F 1 "CONN_01X02" V 5400 3450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 5300 3450 50  0001 C CNN
F 3 "" H 5300 3450 50  0000 C CNN
	1    5300 3450
	1    0    0    -1  
$EndComp
$Comp
L IRFR3708 Q4
U 1 1 5AB93519
P 5500 5950
F 0 "Q4" H 5650 6075 50  0000 L CNN
F 1 "IRFR3708" H 5650 6000 50  0000 L CNN
F 2 "esp32:TO-252" H 5650 5925 50  0000 L CIN
F 3 "" H 5400 6000 50  0000 L CNN
	1    5500 5950
	1    0    0    -1  
$EndComp
$Comp
L IRFR3708 Q3
U 1 1 5AB93552
P 5500 5400
F 0 "Q3" H 5650 5525 50  0000 L CNN
F 1 "IRFR3708" H 5650 5450 50  0000 L CNN
F 2 "esp32:TO-252" H 5650 5375 50  0000 L CIN
F 3 "" H 5400 5450 50  0000 L CNN
	1    5500 5400
	1    0    0    -1  
$EndComp
$Comp
L IRFR3708 Q2
U 1 1 5AB93589
P 5500 4900
F 0 "Q2" H 5650 5025 50  0000 L CNN
F 1 "IRFR3708" H 5650 4950 50  0000 L CNN
F 2 "esp32:TO-252" H 5650 4875 50  0000 L CIN
F 3 "" H 5400 4950 50  0000 L CNN
	1    5500 4900
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P5
U 1 1 5AB936A1
P 6500 5350
F 0 "P5" H 6500 5500 50  0000 C CNN
F 1 "LED2" V 6600 5350 50  0000 C CNN
F 2 "esp32:terminal_block" H 6500 5350 50  0001 C CNN
F 3 "" H 6500 5350 50  0000 C CNN
	1    6500 5350
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P6
U 1 1 5AB936DC
P 6500 5850
F 0 "P6" H 6500 6000 50  0000 C CNN
F 1 "LED1" V 6600 5850 50  0000 C CNN
F 2 "esp32:terminal_block" H 6500 5850 50  0001 C CNN
F 3 "" H 6500 5850 50  0000 C CNN
	1    6500 5850
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P3
U 1 1 5AB93731
P 6450 4250
F 0 "P3" H 6450 4400 50  0000 C CNN
F 1 "LED4" V 6550 4250 50  0000 C CNN
F 2 "esp32:terminal_block" H 6450 4250 50  0001 C CNN
F 3 "" H 6450 4250 50  0000 C CNN
	1    6450 4250
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P4
U 1 1 5AB9378C
P 6450 4850
F 0 "P4" H 6450 5000 50  0000 C CNN
F 1 "LED3" V 6550 4850 50  0000 C CNN
F 2 "esp32:terminal_block" H 6450 4850 50  0001 C CNN
F 3 "" H 6450 4850 50  0000 C CNN
	1    6450 4850
	1    0    0    -1  
$EndComp
$Comp
L +12V #PWR07
U 1 1 5AB9CCDD
P 6700 3850
F 0 "#PWR07" H 6700 3700 50  0001 C CNN
F 1 "+12V" H 6700 3990 50  0000 C CNN
F 2 "" H 6700 3850 50  0001 C CNN
F 3 "" H 6700 3850 50  0001 C CNN
	1    6700 3850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 5AB9CEDB
P 5050 6250
F 0 "#PWR08" H 5050 6000 50  0001 C CNN
F 1 "GND" H 5050 6100 50  0000 C CNN
F 2 "" H 5050 6250 50  0001 C CNN
F 3 "" H 5050 6250 50  0001 C CNN
	1    5050 6250
	1    0    0    -1  
$EndComp
$Comp
L D D4
U 1 1 5AB9CF97
P 6250 6250
F 0 "D4" H 6250 6350 50  0000 C CNN
F 1 "D" H 6250 6150 50  0000 C CNN
F 2 "Diodes_SMD:D_0805" H 6250 6250 50  0001 C CNN
F 3 "" H 6250 6250 50  0000 C CNN
	1    6250 6250
	0    -1   -1   0   
$EndComp
$Comp
L D D3
U 1 1 5AB9D094
P 6200 5600
F 0 "D3" H 6200 5700 50  0000 C CNN
F 1 "D" H 6200 5500 50  0000 C CNN
F 2 "Diodes_SMD:D_0805" H 6200 5600 50  0001 C CNN
F 3 "" H 6200 5600 50  0000 C CNN
	1    6200 5600
	0    -1   -1   0   
$EndComp
$Comp
L D D2
U 1 1 5AB9D136
P 6150 5150
F 0 "D2" H 6150 5250 50  0000 C CNN
F 1 "D" H 6150 5050 50  0000 C CNN
F 2 "Diodes_SMD:D_0805" H 6150 5150 50  0001 C CNN
F 3 "" H 6150 5150 50  0000 C CNN
	1    6150 5150
	0    -1   -1   0   
$EndComp
$Comp
L D D1
U 1 1 5AB9D177
P 6100 4550
F 0 "D1" H 6100 4650 50  0000 C CNN
F 1 "D" H 6100 4450 50  0000 C CNN
F 2 "Diodes_SMD:D_0805" H 6100 4550 50  0001 C CNN
F 3 "" H 6100 4550 50  0000 C CNN
	1    6100 4550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2350 1250 2350 2200
Wire Wire Line
	2000 1400 2800 1400
Connection ~ 2550 1400
Wire Wire Line
	2350 2200 3050 2200
Connection ~ 2350 1400
Wire Wire Line
	3050 2100 2550 2100
Wire Wire Line
	2550 2100 2550 1700
Wire Wire Line
	2550 1700 2800 1700
Wire Wire Line
	2800 1700 2800 1850
Wire Wire Line
	2000 1700 2000 1400
Wire Wire Line
	2000 2000 2000 2250
Wire Wire Line
	2000 2550 2000 2750
Wire Wire Line
	2000 2100 2300 2100
Wire Wire Line
	2300 2100 2300 2300
Wire Wire Line
	2300 2300 3050 2300
Connection ~ 2000 2100
Wire Wire Line
	4800 1950 4800 2200
Wire Wire Line
	4800 1950 5250 1950
Wire Wire Line
	5050 1950 5050 2050
Connection ~ 4800 2100
Wire Wire Line
	4800 2400 5550 2400
Wire Wire Line
	4800 2500 5550 2500
Wire Wire Line
	5100 3700 5100 3500
Wire Wire Line
	5100 3400 4800 3400
Wire Wire Line
	3500 4100 3500 3800
Wire Wire Line
	6250 4300 5850 4300
Wire Wire Line
	5850 4300 5850 4000
Wire Wire Line
	5850 4000 5500 4000
Wire Wire Line
	5500 4000 5500 4050
Wire Wire Line
	5500 4650 5500 4600
Wire Wire Line
	5500 4600 5900 4600
Wire Wire Line
	5900 4600 5900 4900
Wire Wire Line
	5900 4900 6250 4900
Wire Wire Line
	6300 5400 6000 5400
Wire Wire Line
	6000 5400 6000 5100
Wire Wire Line
	6000 5100 5500 5100
Wire Wire Line
	5500 5100 5500 5150
Wire Wire Line
	6100 5900 6300 5900
Wire Wire Line
	6100 5900 6100 5650
Wire Wire Line
	6100 5650 5500 5650
Wire Wire Line
	5500 5650 5500 5700
Wire Wire Line
	6250 4800 6250 4550
Wire Wire Line
	6250 4550 6700 4550
Wire Wire Line
	6700 3850 6700 6400
Wire Wire Line
	6700 4000 6250 4000
Wire Wire Line
	6250 4000 6250 4200
Wire Wire Line
	6300 5300 6300 5100
Wire Wire Line
	6300 5100 6700 5100
Connection ~ 6700 4550
Wire Wire Line
	6300 5800 6300 5550
Wire Wire Line
	6300 5550 6700 5550
Connection ~ 6700 5100
Connection ~ 6700 4000
Wire Wire Line
	5500 4450 5050 4450
Wire Wire Line
	5050 4450 5050 6250
Wire Wire Line
	5500 5550 5050 5550
Connection ~ 5050 5550
Wire Wire Line
	5500 5050 5050 5050
Connection ~ 5050 5050
Wire Wire Line
	6250 6100 6250 5900
Connection ~ 6250 5900
Wire Wire Line
	6700 6400 6250 6400
Connection ~ 6700 5550
Wire Wire Line
	6200 5750 6300 5750
Connection ~ 6300 5750
Wire Wire Line
	6200 5450 6200 5400
Connection ~ 6200 5400
Wire Wire Line
	6150 5300 6300 5300
Wire Wire Line
	6150 5000 6150 4900
Connection ~ 6150 4900
Wire Wire Line
	6100 4700 6250 4700
Connection ~ 6250 4700
Wire Wire Line
	6100 4300 6100 4400
Connection ~ 6100 4300
Wire Wire Line
	5200 4300 5050 4300
Wire Wire Line
	5050 4300 5050 3000
Wire Wire Line
	5050 3000 4800 3000
Wire Wire Line
	4800 3100 5000 3100
Wire Wire Line
	5000 3100 5000 4900
Wire Wire Line
	5000 4900 5200 4900
Wire Wire Line
	5200 5400 4950 5400
Wire Wire Line
	4950 5400 4950 3200
Wire Wire Line
	4950 3200 4800 3200
Wire Wire Line
	4800 3300 4900 3300
Wire Wire Line
	4900 3300 4900 5950
Wire Wire Line
	4900 5950 5200 5950
Wire Wire Line
	5550 2300 5250 2300
Wire Wire Line
	5250 2300 5250 1950
Connection ~ 5050 1950
$Comp
L CONN_01X02 P7
U 1 1 5ABD28B0
P 2200 4200
F 0 "P7" H 2200 4350 50  0000 C CNN
F 1 "POWAH" V 2300 4200 50  0000 C CNN
F 2 "esp32:terminal_block" H 2200 4200 50  0001 C CNN
F 3 "" H 2200 4200 50  0000 C CNN
	1    2200 4200
	-1   0    0    1   
$EndComp
$Comp
L +12V #PWR09
U 1 1 5ABD2C55
P 2550 4000
F 0 "#PWR09" H 2550 3850 50  0001 C CNN
F 1 "+12V" H 2550 4140 50  0000 C CNN
F 2 "" H 2550 4000 50  0001 C CNN
F 3 "" H 2550 4000 50  0001 C CNN
	1    2550 4000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 5ABD2C91
P 2550 4300
F 0 "#PWR010" H 2550 4050 50  0001 C CNN
F 1 "GND" H 2550 4150 50  0000 C CNN
F 2 "" H 2550 4300 50  0001 C CNN
F 3 "" H 2550 4300 50  0001 C CNN
	1    2550 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 4300 2550 4250
Wire Wire Line
	2550 4250 2400 4250
Wire Wire Line
	2400 4150 2550 4150
Wire Wire Line
	2550 4150 2550 4000
$Comp
L LED_ARGB D5
U 1 1 5ABD2FD2
P 4100 5100
F 0 "D5" H 4100 5470 50  0000 C CNN
F 1 "LED_ARGB" H 4100 4750 50  0000 C CNN
F 2 "" H 4100 5050 50  0001 C CNN
F 3 "" H 4100 5050 50  0000 C CNN
	1    4100 5100
	1    0    0    -1  
$EndComp
$Comp
L LM2596_MODULE U2
U 1 1 5ACA6987
P 2300 5350
F 0 "U2" H 2300 5650 60  0000 C CNN
F 1 "LM2596_MODULE" H 2300 5750 60  0000 C CNN
F 2 "esp32:LM2596_MODULE" H 2300 5350 60  0001 C CNN
F 3 "" H 2300 5350 60  0001 C CNN
	1    2300 5350
	1    0    0    -1  
$EndComp
$Comp
L +12V #PWR011
U 1 1 5ACA6DBE
P 1400 5700
F 0 "#PWR011" H 1400 5550 50  0001 C CNN
F 1 "+12V" H 1400 5840 50  0000 C CNN
F 2 "" H 1400 5700 50  0001 C CNN
F 3 "" H 1400 5700 50  0001 C CNN
	1    1400 5700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 5ACA6DFE
P 1450 5300
F 0 "#PWR012" H 1450 5050 50  0001 C CNN
F 1 "GND" H 1450 5150 50  0000 C CNN
F 2 "" H 1450 5300 50  0001 C CNN
F 3 "" H 1450 5300 50  0001 C CNN
	1    1450 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 5300 1450 5200
Wire Wire Line
	1450 5200 1800 5200
Wire Wire Line
	1400 5700 1400 5800
Wire Wire Line
	1400 5800 1650 5800
Wire Wire Line
	1650 5800 1650 5500
Wire Wire Line
	1650 5500 1800 5500
$Comp
L GND #PWR013
U 1 1 5ACA70C8
P 3000 5200
F 0 "#PWR013" H 3000 4950 50  0001 C CNN
F 1 "GND" H 3000 5050 50  0000 C CNN
F 2 "" H 3000 5200 50  0001 C CNN
F 3 "" H 3000 5200 50  0001 C CNN
	1    3000 5200
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR014
U 1 1 5ACA7108
P 3150 5500
F 0 "#PWR014" H 3150 5350 50  0001 C CNN
F 1 "+3V3" H 3150 5640 50  0000 C CNN
F 2 "" H 3150 5500 50  0001 C CNN
F 3 "" H 3150 5500 50  0001 C CNN
	1    3150 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 5500 2800 5500
Wire Wire Line
	3000 5200 2800 5200
Wire Wire Line
	5500 6100 5050 6100
Connection ~ 5050 6100
$EndSCHEMATC
